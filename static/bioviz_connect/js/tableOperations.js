//tabelOperations.js

 var fldrpth=homePath
 var offset=0;
 var searchBack="";
 var page_col="";
 var page_dir="";
function setupPagination(data, enable_prev, enable_next, defaultaction){
        var pages = 0
        var total = parseInt(data["total"])
        var pages_rem = total%recordsLimit
        if (pages_rem == 0)
        {
            pages = parseInt((total/recordsLimit))
        }
        else {
            pages = parseInt((total/recordsLimit)) + 1
        }
        if (pages == 0){
            $('#pagetext').hide();
            $('#page-selection').hide();
            return
        }
        else{
            $('#pagetext').show();
            $('#page-selection').show();
        }
        // logic to enable disable the next and previous
        if (defaultaction) {
            if (pages == 1) {
                enable_prev = false;
        enable_next = false;
            } else if (pages > 1) {
                 enable_next = '»';
                 enable_prev = false;
            }
        }
        else{
            var currentpage = parseInt($("#page-selection").data("currentPage"));
            if (currentpage == 1){
                enable_prev = false;
                enable_next = '»';
                $('#page-selection').bootpag({next: enable_next, prev: enable_prev})
            }
            else if(currentpage == pages){
                   enable_next = false;
                   enable_prev = '«';
                   $('#page-selection').bootpag({next: enable_next, prev: enable_prev})

            }
            else{
                enable_next = '»';
                enable_prev = '«';
                $('#page-selection').bootpag({next: enable_next, prev: enable_prev})
            }
        } // continue from here , to seperate the code.
        if (defaultaction){
        // init bootpag
        $('#page-selection').bootpag({
            total: pages,
            maxVisible: 5,
            next: enable_next,
            prev: enable_prev
        });
        }
            $('ul.pagination.bootpag li').click(function() {
                var num = $(this).attr('data-lp');
                $("#page-selection").data("currentPage", num)
             var section = $("#page-selection").data("currentSection")
             if (section == "home"){
                offset=(num-1)*recordsLimit
                getFileList($("#page-selection").data("currentPath") , recordsLimit, (num-1)*recordsLimit, page_col, page_dir,'home', false)
             }
             else if (section == "shared"){
                offset=(num-1)*recordsLimit
                getFileList($("#page-selection").data("currentPath") , recordsLimit, (num-1)*recordsLimit, page_col, page_dir,'shared', false)
             }
             else if(section == "community"){
                 offset=(num-1)*recordsLimit
                 getFileList($("#page-selection").data("currentPath") , recordsLimit, (num-1)*recordsLimit,page_col, page_dir,'community', false)
             }
         else if(section == "Analyses"){
             analysesHistory(recordsLimit, (num-1)*recordsLimit, false)
         }
    });
}

function interpretData(data, functiontype){
    $('#fileFolder').children('tbody').html("")
    if(data.total<=0){
        $("#pagetext").hide();
        var empty = "<tr><td colspan='4'>\n"+
        "<div class='d-flex justify-content-center alert-primary m-2 p-3 rounded'>\n"+
        "This folder is empty. Use the 'New' menu to upload files or create folders\n"+
        "</div></td></tr>"
        $('#fileFolder').children('tbody').html(empty)
	}else{
        populateTable(data["folders"], "folders")
        populateTable(data["files"], "files")
        updateSearchFilePath();
    }
    $("tr").on({contextmenu:function(e) {
            $("tr").removeClass('active');
            $(this).addClass('active');
            if ($(this).hasClass("file").toString() == "true") {
                        permission_tr = $(this).data("info").permission
                        ispublic_tr = $(this).data("info").isPublic
                        e.preventDefault();
                        $("#context-menu").data("fileInfo",$(this).data("info"));
                        $("#context-menu").data("fileType","file");
                        $('#context-menu').css({'display': 'block', 'left': e.pageX, 'top': e.pageY});
                        $('#popup-menu').css({'display':'none'});
                        showhide_rightpanel_options(permission_tr, ispublic_tr, false);
            }
            else if ($(this).hasClass("folder").toString() == "true") {
                        permission_tr = $(this).data("info").permission
                        ispublic_tr = $(this).data("info").isPublic
                        e.preventDefault();
                        $("#context-menu").data("fileInfo",$(this).data("info"));
                        $("#context-menu").data("fileType","folder");
                        $('#context-menu').css({'display': 'block', 'left': e.pageX, 'top': e.pageY});
                        $('#popup-menu').css({'display':'none'});
                        showhide_rightpanel_options(permission_tr, ispublic_tr, true);
            }

            if (rpStatus == 1){

                if ($(".right").data("fileInfo")){
                    previous_path = $(".right").data("fileInfo").path
                    current_path = $(this).data("info").path

                    if (previous_path != current_path){
                        $(".right").data("fileInfo", $(this).data("info"))
                        rightPanel($(".right").data("currentrightpanel"))
                    }
                }
                else{
                    $(".right").data("fileInfo", $(this).data("info"))
                }

            }
            else{
                $(".right").data("fileInfo", $(this).data("info"))
            }


        },click:function(e){
            $("tr").removeClass('active');
            $(this).addClass('active');
            if ($(this).hasClass("file").toString() == "true") {
                        if (rpStatus == 1)
                        {
                            previous_path = $(".right").data("fileInfo").path
                            current_path = $(this).data("info").path
                            if (previous_path != current_path) {
                                $("#pop_metadata").data("isfolder", false)
                                $(".right").data("fileInfo", $(this).data("info"))
                                rightPanel($(".right").data("currentrightpanel"))
                            }
                        }
                        else{
                            $(".right").data("fileInfo", $(this).data("info"))
                        }
                    } // code for folders
                    if ($(this).hasClass("folder").toString() == "true") {
                        fldrpth = $(this).data("info").path;
                        if (rpStatus == 1) {
                            previous_path = $(".right").data("fileInfo").path
                            current_path = $(this).data("info").path
                            if (previous_path != current_path) {
                                $("#pop_metadata").data("isfolder", true)
                                $(".right").data("fileInfo", $(this).data("info"))
                                rightPanel($(".right").data("currentrightpanel"))
                            }
                        }
                        else{
                                $(".right").data("fileInfo", $(this).data("info"))
                        }
                    }
        },dblclick:function(e){
            $("#searchBackBtn").css("visibility","hidden")
            $("tr").removeClass('active');
            $(this).addClass('active');
            $(".right").data("fileInfo",$(this).data("info"))
            if($(this).hasClass("file")) {
                $("#context-menu").data("fileInfo",$(this).data("info"));
                rightPanel('#metadata')
            }
            if($(this).hasClass("folder").toString()=="true") {
                $("#searchRender").data("enableSearchRender", false)
                fldrpth =$(this).data("info").path;
                index = 0
                var res = fldrpth.split("/");

                res.forEach(function (e) {
                    encodedfilename = encodeURIComponent(e)
                    res[index] = encodedfilename
                    index = index + 1
                });

                encodedfilepath = res.join('/')

                location.hash= encodedfilepath
            }
        }
    });

    $("div").on('scroll',function(){
        $('#popup-menu').css({'display':'none'});
        $('#context-menu').css({'display':'none'});
        $('#popup-bc-paste').css({'display':'none'});
    });


    $(".popMenu").on({click:function(e) {
        var isFolder = false
        $("tr").removeClass('active');
        var prntRow=$(this).closest("tr");
        if(prntRow.hasClass("folder").toString()=="true"){
                isFolder = true
        }
        else{
                isFolder = false
        }
        permission_tr = prntRow.data("info").permission
        ispublic_tr = prntRow.data("info").isPublic
        prntRow.addClass('active');
        showhide_rightpanel_options(permission_tr, ispublic_tr, isFolder)
        $('#popup-menu').css({'display':'block' ,'left': e.pageX, 'top': e.pageY});
            //$(".right").data("fileInfo",prntRow.data("info"))
       }
    });

    $('body').click(function(evt){
       if($(evt.target).closest("button").attr("class") != "btn popMenu"){
            $('#popup-menu').css({'display':'none'});
       }
       if(evt.target.id != "#context-menu"){
            $('#context-menu').css({'display':'none'});
       }
       if(evt.target.id != "#popup-bc-paste"){
            $("#popup-bc-paste").css({'display':'none'});
       }
    });
    imposeRenameUI()
}

function toggledisable_metadata(is_disable){
            $('#dropdownMenuButtonSp').attr('disabled', is_disable);
            $('#dropdownMenuButtonSp').attr('disabled', is_disable)
            $('#dropdownMenuButtonVn').attr('disabled', is_disable)
            $('#foregroundcp').attr('disabled', is_disable)
            $('#backgroundcp').attr('disabled', is_disable)
            $('#closemetadata_rp').attr('disabled', is_disable)
            $('#savemetadata_rp').attr('disabled', is_disable)
            $('#comments').attr('disabled', is_disable)
}

function toggledisable_managelink(is_disable){
    $("#remove").attr('disabled', is_disable)
    $("#create").attr('disabled', is_disable)
}
function clearAllMenuOptions(){
	 $('#pop_analyse').removeClass('disabled')
	 $('#pop_manage').removeClass('disabled')
	 $('#pop_createFolder').removeClass('disabled');
	 $('#pop_rename').removeClass('disabled');
	 $('#pop_metadata').removeClass('disabled');
	 $('#pop_copyPath').removeClass('disabled');
	 $('#pop_download').removeClass('disabled');
     $('#pop_cut').removeClass('disabled');
     $('#pop_delete').removeClass('disabled');

	 $('#pop_analyse').removeClass('text-secondary')
	 $('#pop_manage').removeClass('text-secondary')
	 $('#pop_createFolder').removeClass('text-secondary');
	 $('#pop_rename').removeClass('text-secondary');
	 $('#pop_metadata').removeClass('text-secondary');
	 $('#pop_copyPath').removeClass('text-secondary');
	 $('#pop_download').removeClass('text-secondary');
     $('#pop_cut').removeClass('text-secondary');
     $('#pop_delete').removeClass('text-secondary');
}
function showhide_rightpanel_options(permission_tr, ispublic_tr, is_folder){
	clearAllMenuOptions();
    if (ispublic_tr==true){
        if(permission_tr=='own'){
            toggledisable_metadata(false)
            toggledisable_managelink(false)
        }
        else if(permission_tr=='read'){
            $('#pop_rename').addClass('disabled');
            $('#pop_rename').addClass('text-secondary');
            $('#pop_cut').addClass('disabled');
            $('#pop_cut').addClass('text-secondary');
            $('#pop_delete').addClass('disabled');
            $('#pop_delete').addClass('text-secondary');
            toggledisable_metadata(true)
            toggledisable_managelink(true)
        }
        else if(permission_tr=='write'){
            toggledisable_metadata(false)
            toggledisable_managelink(true)

        }
    }
    else{
        if(permission_tr=='own'){
            toggledisable_metadata(false)
        }
        else if(permission_tr=='read'){
            toggledisable_metadata(true)
            toggledisable_managelink(true)
        }
        else if(permission_tr=='write'){
            toggledisable_metadata(false)
            toggledisable_managelink(true)
        }
    }

    if(is_folder){
        $("#pop_metadata").data("isfolder", true)
        $('#pop_analyse').addClass('disabled')
		$('#pop_analyse').addClass('text-secondary')
		$('#pop_download').addClass('disabled')
		$('#pop_download').addClass('text-secondary')
    }
    else{
        $("#pop_metadata").data("isfolder", false)
    }
}
function buildBreadCrumbs(data,functiontype) {
    $(".breadcrumb").html('')
    $(".breadcrumb").data("currentPath",data.fileSystemInfo.path)
    var link = ""
    var prefix = ""
    if (functiontype == 'getFileList') {
        var directories = data.fileSystemInfo.path.split(homePath)[1]
        var prefix = homePath
        $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#' + homePath+ '\' onclick="disableSearchRender();"><i class="fas fa-home mr-1"></i></a></li>')
    }
    else if (functiontype == 'getShareFileList') {
        var directories = data.fileSystemInfo.path.split(homePathforShare)[1]
        var prefix = homePathforShare
        $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#' + homePathforShare+ '\' onclick="disableSearchRender(); location.hash = homePathforShare;"><i class="fas fa-user-friends mr-1"></i></a></li>')
    } else if (functiontype == 'getCommunityData') {
        var directories = data.fileSystemInfo.path.split(homePathforCommunity)[1]
        var prefix = homePathforCommunity
        $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href = \'#' + homePathforCommunity+ '\' onclick="disableSearchRender(); location.hash = homePathforCommunity"><i class="fas fa-users mr-1"></i></a></li>')
    }
    if(directories!=null && directories!=''){
        link=""
        for (var dir in directories.split('/')) {
            if(directories.split('/')[dir] == '')
                continue
            appenddir = encodeURIComponent(directories.split('/')[dir])
            link = link+"/"+appenddir
            $(".breadcrumb").append('<li class=\"breadcrumb-item text-truncate\"><a href=\'#' + prefix+link + '\' onclick="disableSearchRender()" >' + directories.split('/')[dir] + '</a></li>')
        }
    }
    $(".breadcrumb-item").on({contextmenu:function(e) {
        e.preventDefault();
        var storage = window.sessionStorage;
        var cut_path_list = JSON.parse(storage.getItem("cut_path_list"))
	    var cut_id_list = JSON.parse(storage.getItem("cut_id_list"))
	    if(cut_path_list != null){
	        $("#popup-bc-paste").css({'display':'block' ,'left': e.pageX, 'top': e.pageY});
	        $("#popup-bc-paste").data("path",$(this).find("a").attr("href").replace("#",""))
	    }

    }})
    if(link!="")
         var toastcopy = '<li>'+
            '<div class="input-group-append">'+
                '<button class="btn btn-sm far fa-copy ml-3" onclick="copyBreadCrumbPath(\'' + prefix+directories + '\')" ></button>'+
                '<div class="toast copiedPathToast mt-5 hide" role="alert" data-autohide="true" data-delay="500">'+
                '<div class="toast-header">'+
                    '<i class=" far fa-copy fa-bounce fa-lg  mr-2"></i>'+
                    '<strong>Path Copied!</strong>'+
            '</div></div></div></li>'
        $(".breadcrumb").append(toastcopy)

}
function copyBreadCrumbPath(path){
	var dummy = document.createElement("textarea");
	document.body.appendChild(dummy);
	dummy.value = path;
	dummy.select();
	document.execCommand("copy");
	document.body.removeChild(dummy);
	$(".copiedPathToast").toast("show");

}

