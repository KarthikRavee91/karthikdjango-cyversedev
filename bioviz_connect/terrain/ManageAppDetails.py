import requests
from .. import models
from django.http import HttpResponse
import json
import uuid

from ..models import AppDetails
from ..terrain_model.CustomEncoder import CustomEncoder

def DeleteAppData():
    models.Arguments.objects.all().delete()
    models.InputDetails.objects.all().delete()
    models.AppDetails.objects.all().delete()
    models.igbfileformats.objects.all().delete()


def getLatestApps(accesstoken):
    custom_encoder = CustomEncoder()
    req_url = 'https://de.cyverse.org/terrain/apps/communities/iplant:de:prod:communities:Integrated Genome Browser/apps'
    try:
        r = requests.get(req_url, headers={'Authorization': 'BEARER ' + accesstoken})
    except TypeError as e:
        return HttpResponse(
            "TypeError: " + e.__str__() + "\nPlease make sure that the accesstoken is being passed with the request call.")
    except Exception as e:
        return e
    if 'error_code' in r.json():
        if (r.json()['error_code'] == 'ERR_NOT_AUTHORIZED'):
            return (r.json()['error_code'] + "\nPlease make sure that the accesstoken is valid")
        else:
            return ("Error: ", r.json()['error_code'])
    DeleteAppData()
    apps = add_IGBApps(r.json(), accesstoken)
    add_igbfileformats()
    return custom_encoder.encode(apps)


def add_igbfileformats():
    files_supported = 'cnchp,lohchp,bar,gff3,gtf,bgn,bgr,bp1,bps,brpt,brs,bsnp,chp,cnt,cyt,ead,fa,fas,fasta,fna,fsa,mpfa,fsh,gb,gen,gr,link.psl,bnib,sin,egr,egr.txt,map,cn_segments,loh_segments,sgr,2bit,useq,var,wig,bdg,bedgraph,vcf,sam,bed,gff,psl,psl3,pslx,bam,axml,bigbed,bb,bw,bigWig,bigwig,das,dasxml,das2xml,narrowPeak,narrowpeak,broadPeak,broadpeak,tally,gz'
    files_supported_list = str.split(files_supported, ',')
    for filetype in files_supported_list:
        fileformat = models.igbfileformats()
        fileformat.id = str(uuid.uuid4())
        fileformat.filetype = filetype
        fileformat.save()

def formatFileTypes_fromdescription(descrption):
    start_indexFileType = descrption.find('%INPUT%')
    if start_indexFileType == -1:
        return ""
    return descrption[start_indexFileType+7:]

def get_InputDetailsForApp(appId, accesstoken):
    req_url = 'https://de.cyverse.org/terrain/apps/de/'+appId
    r = requests.get(req_url, headers={'Authorization': 'BEARER ' + accesstoken})
    return r.json()

def getCompatibleAppsByformat(Format):
    custom_encoder = CustomEncoder()
    appdetails = list(models.AppDetails.objects.filter(Formats__contains=Format))
    return custom_encoder.encode(appdetails)
def getAllAppsFormats():
    custom_encoder = CustomEncoder()
    appdetails = list(models.AppDetails.objects.values_list('Formats', flat=True))
    appdetails = [*set(appdetails)]
    return custom_encoder.encode(appdetails)

def Add_InputDetailsForApp(AppId, accesstoken):
    app_description = get_InputDetailsForApp(AppId, accesstoken)
    for group in app_description['groups']:
        label = group['label']
        for parameter in group['parameters']:
            inputdetails = models.InputDetails()
            inputdetails.Description = parameter['description']
            inputdetails.Formtype = parameter['type']
            inputdetails.AppId = AppDetails.objects.get(id = AppId)
            inputdetails.id = parameter['id']
            inputdetails.label = parameter['label']
            inputdetails.Inputtype = label
            if 'defaultValue' in parameter:
                inputdetails.DefaultValue = parameter['defaultValue']
            else:
                inputdetails.DefaultValue = ''
            inputdetails.save()
            if len(parameter['arguments'])>0:
                for argument in parameter['arguments']:
                    arguments = models.Arguments()
                    arguments.id = argument['id']
                    arguments.isDefault = bool(argument['isDefault'])
                    arguments.Name = argument['name']
                    arguments.value = argument['value']
                    arguments.Display = argument['display']
                    arguments.InputDetailId = inputdetails.id
                    arguments.save()

def add_IGBApps(json, accesstoken):
    apps = []
    for app_json in json['apps']:
        app = models.AppDetails()
        app.id = app_json['id']
        app.Name = app_json['name']
        app.description = app_json['description']
        app.Formats = formatFileTypes_fromdescription(app_json['description'])
        app.save()
        apps.append(app)
        Add_InputDetailsForApp(app.id, accesstoken)
    return apps

def getAppDataWithAppId(AppId):
    appData = []
    print("appData")
    inputfields =models.InputDetails.objects.filter(AppId=AppId)
    for num,field in enumerate(inputfields,start=0):
        appData.append({'id': field.id,
                        'type':field.Formtype,
                        'description': field.Description,
                        'label': field.label,
                        'inputType': field.Inputtype,
                        'formType': field.Formtype})
    return appData

def getNameForApp(AppId):
    appname = models.AppDetails.objects.filter(id=AppId)[0]
    return appname.Name



