# Print Your code here
import requests
from .. import settings
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder
##


def getSharedData(path, limit, offset, sortcol, sortdir, redis_sessiondata):
    try:
        accesstoken = str(redis_sessiondata[0])
        json_data_user = str(redis_sessiondata[1])
        req_url = 'https://de.cyverse.org/terrain/secured/filesystem/paged-directory?path='+path+'&limit='+limit+'&offset='+offset+'&sort-col='+sortcol+'&sort-dir='+sortdir
        r = requests.get(req_url, headers={'Authorization': 'BEARER ' + accesstoken})
        json_data = r.json()

        if 'error_code' in json_data:
            if (json_data['error_code'] == 'ERR_NOT_AUTHORIZED'):
                raise PermissionError()

        response = ResponseParser.get_files(json_data, "shared", accesstoken)
        custom_encoder = CustomEncoder()
        res = custom_encoder.encode(response)
        return res
    except PermissionError as error:
        raise PermissionError()



#getSharedData('/iplant/home', '5', '0', 'NAME','ASC')
