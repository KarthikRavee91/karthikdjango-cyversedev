import math
from datetime import datetime
from  .RequestCreator import RequestCreator
from .FileData import FileData
from .FileSystemInfo import FileSystemInfo
from .ShareResponse import ShareResponse
from .Sharing import Sharing
from .SharingPath import SharingPath
from .Unshare import Unshare
from .UnshareResponse import UnshareResponse
from .RetrieveAnalysesResponse import RetrieveAnalysesResponse
from .Analyses import Analyses
from .CoverageGraphAnalysisResponse import CoverageGraphAnalysisResponse
from .FilePermissionPath import FilePermissionPath
from .FilePermissionResponse import FilePermissionResponse
from .UserPermission import UserPermission
from . import FileIDInfo
from .MetaData import MetaData
from . import MetaDataView
from .UserDetails import UserDetails
from .basepaths import basepaths
from .Subjects import Subjects
from .UnSharing import UnSharing
from .AccessToken import AccessToken
import requests
from .. import settings
import ast
import json
from .. import models


class ResponseParser:
    # Parses the response received from the apis
    # Converts them into more structured format

    @staticmethod
    def parse_response(response, section, accesstoken):
        file_data = FileData()
        file_data.hasSubDirs = response['hasSubDirs']
        file_data.total = response['total']
        file_data.totalBad = response['totalBad']
        file_data.folders = ResponseParser.parse_folders(response['folders'], section, accesstoken)
        file_data.files = ResponseParser.parse_files(response['files'], section, accesstoken)
        file_data.fileSystemInfo = ResponseParser.parse_file_system_info(response, section, accesstoken)
        return file_data


    @staticmethod
    def isViewable(filetype):
        filetypes = models.igbfileformats.objects.all()
        listoftypes = []
        for types in filetypes:
            listoftypes.append(types.filetype)
        if (filetype.lower() in listoftypes):
            return True
        else:
            return False

    @staticmethod
    def parse_userDetailresponse(response):
        details = UserDetails()
        for root in response['roots']:
            fileinfo = FileIDInfo.FileIDInfo()
            fileinfo.id = root['id']
            fileinfo.path = root['path']
            fileinfo.label = root['label']
            fileinfo.datecreated = root['date-created']
            fileinfo.datemodified = root['date-modified']
            fileinfo.permission = root['permission']
            fileinfo.hasSubDirs = root['hasSubDirs']
            details.roots.append(fileinfo)

        basepaths_json = response['base-paths']
        basepath_obj = basepaths()
        basepath_obj.user_trash_path = basepaths_json['user_trash_path']
        basepath_obj.user_home_path =  basepaths_json['user_home_path']
        basepath_obj.base_trash_path = basepaths_json['base_trash_path']
        details.base_paths = basepath_obj
        return details

    @staticmethod
    def parse_fileIDresponse(response):
        fileID_data = FileIDInfo.FileIDInfo()
        path = ''
        for path_file in response['paths']:
            path = path_file
        if 'paths' in response:
            response_path = response['paths'][path]
        if 'infoType' in response_path:
            fileID_data.infoType = response_path['infoType']
        if 'path' in response_path:
            fileID_data.path = response_path['path']
        if 'share-count' in response_path:
            fileID_data.sharecount = response_path['share-count']
        if 'date-created' in response_path:
            fileID_data.datecreated = ResponseParser.convert_date(response_path['date-created'])
        if 'md5' in response_path:
            fileID_data.md5 = response_path['md5']
        if 'permission' in response_path:
            fileID_data.permission = response_path['permission']
        if 'date-modified' in response_path:
            fileID_data.datemodified = ResponseParser.convert_date(response_path['date-modified'])
        if 'type' in response_path:
            fileID_data.type = response_path['type']
        if 'file-size' in response_path:
            fileID_data.filesize = response_path['file-size']
        if 'label' in response_path:
            fileID_data.label = response_path['label']
        if 'id' in response_path:
            fileID_data.id = response_path['id']
        if 'content-type' in response_path:
            fileID_data.contenttype = response_path['content-type']
        if 'ids' in response_path:
            fileID_data.ids = response['ids']
        if 'dir-count' in response_path:
            fileID_data.dircount = response_path['dir-count']
        if 'file-count' in response_path:
            fileID_data.filecount = response_path['file-count']
        return fileID_data

    @staticmethod
    def parse_MetaDataresponse(response):
        avusmodel = MetaDataView.avus()
        metadata_list = []
        if 'avus' in response:
            for avus in response['avus']:
                if 'unit' in avus:
                    if avus['unit'] == 'integratedGenomeBrowser':
                        metaData = MetaData()
                        if 'attr' in avus:
                            metaData.attr = avus['attr']
                        if 'value' in avus:
                            metaData.value = avus['value']
                        if 'id' in avus:
                            metaData.id = avus['id']
                        if 'target_id' in avus:
                            metaData.target_id = avus['target_id']
                        if 'modified_by' in avus:
                            metaData.modified_by = avus['modified_by']

                        metaData.unit = avus['unit']
                        metadata_list.append(metaData)
            avusmodel.avus = metadata_list
            return avusmodel

    @staticmethod
    def parse_individual_search_item(folder,perm,isFile):
        file_info = FileSystemInfo()
        file_info.path = folder['path']
        file_info.dateCreated = folder['dateCreated']
        file_info.dateModified = folder['dateModified']
        file_info.permission = perm[0]
        if 'fileSize' in folder:
            file_info.fileSize = ResponseParser.size_conversion(folder['fileSize'])
        else:
            file_info.fileSize = 0
        file_info.label = folder['label']
        file_info.id = folder['id']
        if isFile:
            if (len(str.split(file_info.label, '.'))>1):
                filetype = str.split(file_info.label, '.')[-1]
                file_info.isViewable = ResponseParser.isViewable(filetype)
        return file_info

    @staticmethod
    def parse_search_file_system_info(response, type, base_homepath, base_community, username):
        file_info = FileSystemInfo()
        if type.lower() == 'home':
            file_info.path = base_homepath + username
        elif type.lower() == 'community':
            file_info.path = base_community
        elif type.lower() == 'shared':
            file_info.path = base_homepath
        return file_info

    @staticmethod
    def parse_filesearchResponse(response, type,  base_homepath, base_community, username):
        file_data = FileData()
        file_data.total = response['total']
        folders = []
        files = []
        for iter in response['hits']:
            if iter['_type'] == 'folder':
                folders.append(ResponseParser.parse_individual_search_item(iter['_source'],iter['fields']['permission'],False))
            elif iter['_type'] == 'file':
                files.append(ResponseParser.parse_individual_search_item(iter['_source'],iter['fields']['permission'],True))
        file_data.folders = folders
        file_data.files = files
        file_data.fileSystemInfo = ResponseParser.parse_search_file_system_info(response, type,  base_homepath, base_community, username)
        return file_data

    @staticmethod
    def parse_searchResponse(response, type,  base_homepath, base_community, username):
        for match in response['hits']:
            match['_source']['dateModified'] = ResponseParser.convert_date(match['_source']['dateModified'])
            match['_source']['dateCreated'] = ResponseParser.convert_date(match['_source']['dateCreated'])
        return ResponseParser.parse_filesearchResponse(response, type,  base_homepath, base_community, username)

    @staticmethod
    def parse_subjects(response):
        subjects = []
        for subject in response['subjects']:
            sub = Subjects.Subjects()
            sub.institution = subject['institution']
            sub.description = subject['description']
            sub.email = subject['email']
            sub.first_name = subject['first_name']
            sub.name = subject['name']
            sub.id = subject['id']
            sub.last_name = subject['last_name']
            sub.source_id = subject['source_id']
            sub.display_name = subject['display_name']
            subjects.append(sub)
        return subjects



    @staticmethod
    def parse_file_system_info(response, section, accesstoken):
        file_info = FileSystemInfo()
        file_info.infoType = response['infoType']
        file_info.path = response['path']
        file_info.dateCreated = ResponseParser.convert_date(response['date-created'])
        file_info.permission = response['permission']
        file_info.dateModified = ResponseParser.convert_date(response['date-modified'])
        file_info.fileSize = ResponseParser.size_conversion(response['file-size'])
        file_info.badName = response['badName']
        file_info.isFavorite = response['isFavorite']
        file_info.label = response['label']
        if (len(str.split(file_info.label, '.'))>1):
            filetype = str.split(file_info.label, '.')[-1]
            file_info.isViewable = ResponseParser.isViewable(filetype)
        file_info.id = response['id']

        return file_info

    @staticmethod
    def parse_files(files_data, section, accesstoken):
        files = []
        for f in files_data:
            if f is not None:
                file = ResponseParser.parse_file_system_info(f, section, accesstoken)
            files.append(file)
        return files

    @staticmethod
    def parse_folders(folders_data, section, accesstoken):
        folders = []
        for f in folders_data:
            if f is not None:
                folder = ResponseParser.parse_file_system_info(f, section, accesstoken)
            folders.append(folder)
        return folders

    @staticmethod
    def size_conversion(file_size):
        size_unit = ['Bytes', 'KB', 'MB', 'GB', 'TB']
        if file_size == 0:
            return '0 Byte'
        i = int(math.floor(math.log(file_size) / math.log(1024)))
        size = round((file_size / math.pow(1024, i)), 2)
        return str(size) + " " + size_unit[i]

    @staticmethod
    def convert_date(time):
        if time > 0:
            datetimeInfo = datetime.fromtimestamp(time / 1000.0).strftime("%m-%d-%Y %H:%M:%S")
            return datetimeInfo

    @staticmethod
    def parse_share_response(response):
        sharing_res = []
        for res in response:
            if res is not None:
                sharing = Sharing()
                sharing.user = res['user']
                sharing.paths = ResponseParser.parse_sharing_path_response(res['sharing'])
                sharing_res.append(sharing)
        return sharing_res

    @staticmethod
    def parse_sharing_path_response(path_response):
        public_generic_url = "https://data.cyverse.org/dav-anon"
        sharing_paths = []
        if path_response is not None:
            for response in path_response:
                if response is not None:
                    p = SharingPath()
                    p.path = public_generic_url + response['path']
                    p.permission = response['permission']
                    p.success = response['success']
                    sharing_paths.append(p)
        return sharing_paths

    @staticmethod
    def parse_unshare_response(response):
        unsharing_res = []
        for res in response:
            if res is not None:
                sharing = UnSharing()
                sharing.user = res['user']
                sharing.unshare = ResponseParser.parse_unsharing_path_response(res['unshare'])
                unsharing_res.append(sharing)
        return unsharing_res

    @staticmethod
    def parse_unsharing_path_response(path_response):
        public_generic_url = "https://data.cyverse.org/dav-anon"
        unsharing_paths = []
        if path_response is not None:
            for response in path_response:
                if response is not None:
                    u = Unshare()
                    u.path = public_generic_url + response['path']
                    u.success = response['success']
                    unsharing_paths.append(u)
        return unsharing_paths

    @staticmethod
    def parse_analyses_data(analyses_response):
        analyses_data = []
        if analyses_response is not None:
            for response in analyses_response:
                if response is not None:
                    analyses = Analyses()
                    analyses.description = response['description']
                    analyses.name = response['name']
                    analyses.can_share = response['can_share']
                    analyses.username = response['username']
                    analyses.app_id = response['app_id']
                    analyses.system_id = response['system_id']
                    analyses.app_disabled = response['app_disabled']
                    analyses.batch = response['batch']
                    analyses.enddate = ResponseParser.convert_date(int(response['enddate']))
                    analyses.status = response['status']
                    analyses.id = response['id']
                    analyses.startdate = ResponseParser.convert_date(int(response['startdate']))
                    if 'app_description' in response:
                        analyses.app_description = response['app_description']
                    analyses.notify = response['notify']
                    analyses.resultfolderid = response['resultfolderid']
                    if 'app_name' in response:
                        analyses.app_name = response['app_name']
                analyses_data.append(analyses)
        return analyses_data

    @staticmethod
    def parse_filepermission_paths(path_response):
        file_permission_paths = []
        if path_response is not None:
            for path_data in path_response:
                if path_data is not None:
                    permission_path = FilePermissionPath()
                    permission_path.path = path_data['path']
                    permission_path.user_permissions = ResponseParser.parse_permissions(path_data['user-permissions'])
                    file_permission_paths.append(permission_path)
        return file_permission_paths

    @staticmethod
    def parse_each_Search(response):
        file_permission_paths = []
        if response is not None:
            for hit in response['hits']:
                permission_path = FilePermissionPath()
                permission_path.path = hit['_source']['path']
                permission_path.user_permissions = ResponseParser.parse_permissions(hit['_source']['userPermissions'])
                file_permission_paths.append(permission_path)
        return file_permission_paths

    @staticmethod
    def parse_permissions(permission_response):
        user_permissions = []
        if permission_response is not None:
            for permission in permission_response:
                if permission is not None:
                    user_permission = UserPermission()
                    user_permission.user = permission['user']
                    user_permission.permission = permission['permission']
                    user_permissions.append(user_permission)
        return user_permissions

    @staticmethod
    def get_files(api_response, section, accesstoken):
        # Parses get file list api
        response = ResponseParser.parse_response(api_response, section, accesstoken)
        return response

    @staticmethod
    def parse_getUserDetails(api_response):
        # Parses get user detail api
        if isinstance(api_response, str):
            api_response = ast.literal_eval(api_response)
        response = ResponseParser.parse_userDetailresponse(api_response)
        return response

    @staticmethod
    def parse_getFileID(api_response):
        # Parses get fileID api
        response = ResponseParser.parse_fileIDresponse(api_response)
        return response

    @staticmethod
    def parse_getMetaData(api_response):
        # Parses get fileID api
        response = ResponseParser.parse_MetaDataresponse(api_response)
        return response

    @staticmethod
    def share_response(response):
        # Parses share response
        share_data = ShareResponse()
        if response is not None:
            for res in response.values():
                if res is not None:
                    share_data.sharing = ResponseParser.parse_share_response(res)
        return share_data

    @staticmethod
    def unshare_response(response):
        #Parses unshare file response
        unshare_data = UnshareResponse()
        if response is not None:
            for res in response.values():
                unshare_data.unshare = ResponseParser.parse_unshare_response(res)
        return unshare_data

    @staticmethod
    def retrieve_analyses_history(response):
        # Parse the retrieved Analyses History
        analyses_data = RetrieveAnalysesResponse()
        if response is not None:
            analyses_data.analyses = ResponseParser.parse_analyses_data(response['analyses'])
            analyses_data.timestamp = ResponseParser.convert_date(int(response['timestamp']))
            analyses_data.total = response['total']
        return analyses_data

    @staticmethod
    def parse_coveragegraph_analysis_response(response):
        # Parse cover graph analysis reponse
        coveragegraph_response = CoverageGraphAnalysisResponse()
        coveragegraph_response.id = response['id']
        coveragegraph_response.name = response['name']
        coveragegraph_response.status = response['status']
        coveragegraph_response.start_date = response['start-date']
        return coveragegraph_response

    @staticmethod
    def parse_file_permission_response(response):
        # parse file permission response
        permission_response = FilePermissionResponse()
        permission_response.paths = ResponseParser.parse_filepermission_paths(response['paths'])
        return response

    @staticmethod
    def parse_search_isPublic_response(response):
        permission_response = FilePermissionResponse()
        permission_response.paths = ResponseParser.parse_each_Search(response)
        return permission_response

    @staticmethod
    def parse_AccessToken(accesstoken):
        accesstoken = AccessToken(accesstoken)
        return accesstoken


