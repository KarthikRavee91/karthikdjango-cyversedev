from . import SortRequest

class SearchFileModelRequest:

    def __init__(self,  size, offset, sort_order, sort_field):
        self.query = None
        self.size = size
        self._from = offset
        self.sort = [SortRequest.SortRequest(sort_order, sort_field)]

