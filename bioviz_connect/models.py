from django.db import models


class AppDetails(models.Model):
    id = models.CharField(max_length=150, primary_key=True, blank=False)
    description = models.TextField(max_length=5000, blank=False)
    Name = models.TextField(max_length=5000, blank=False)
    Formats = models.TextField(max_length=5000, null=True)

    def __str__(self):
        return "%s" % self.id

    class Meta:
        db_table = "AppDetails"


class InputDetails(models.Model):
    id = models.CharField(max_length=500, primary_key=True)
    AppId = models.ForeignKey(AppDetails, on_delete=models.CASCADE, db_column='AppId')
    Description = models.TextField(max_length=5000, null=True)
    label = models.TextField(max_length=5000, null=True)
    Inputtype = models.TextField(max_length=5000, null=True)
    Formtype = models.TextField(max_length=5000, null=True)
    DefaultValue = models.TextField(max_length=5000, null=True)
    htmldata = models.TextField(null=True)

    def __str__(self):
        return "%s" % self.AppId

    class Meta:
        db_table = "InputDetails"


class Arguments(models.Model):
    id = models.CharField(max_length=500, primary_key=True)
    Name = models.TextField(max_length=5000, null=True)
    isDefault = models.BooleanField()
    Display = models.TextField(max_length=5000, null=True)
    value = models.TextField(max_length=5000, null=True)
    InputDetailId = models.ForeignKey(InputDetails, on_delete=models.CASCADE, db_column="InputDetailId")

    class Meta:
        db_table = "Arguments"


class igbfileformats(models.Model):
    id = models.CharField(max_length=500, primary_key=True)
    filetype = models.TextField(max_length=5000, null=True)

    class Meta:
        db_table = "igbfileformats"